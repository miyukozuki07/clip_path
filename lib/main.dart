import 'package:flutter/material.dart';

void main() => runApp(MyInterfaz());


class MyInterfaz extends StatefulWidget {
  @override
  State<MyInterfaz> createState() => _MyInterfazState();
}

class _MyInterfazState extends State<MyInterfaz>{
  @override
  Widget build(BuildContext context) =>MaterialApp(
      home: Scaffold(
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(30),
          child: ClipPath(
            clipper: MyCustomClipper(),
            child: Image(
                image: NetworkImage('https://www.ilustraideas.com/wp-content/uploads/2021/11/dibujando-a-sailor-moon-wallpaper.png')
            ),
          ),
        ),
      ),
  );
}


class MyCustomClipper extends CustomClipper<Path>{
  @override
  Path getClip(Size size){
    Path path = Path()
      ..lineTo(0, 0)
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height)
      ..lineTo(size.width, size.height);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;

}
